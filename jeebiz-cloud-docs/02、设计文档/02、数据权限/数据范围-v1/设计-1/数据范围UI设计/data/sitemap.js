﻿var sitemap =
    {
        "rootNodes": [
            {
                "pageName": "数据范围管理首页",
                "type": "Wireframe",
                "url": "数据范围管理首页.html",
                "children": [
                    {
                        "pageName": "批量数据授权",
                        "type": "Wireframe",
                        "url": "批量数据授权.html"
                    },
                    {
                        "pageName": "单个数据授权",
                        "type": "Wireframe",
                        "url": "单个数据授权.html"
                    }]
            }]
    };
