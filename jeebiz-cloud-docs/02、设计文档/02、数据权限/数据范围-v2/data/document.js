﻿$axure.loadDocument(
    (function () {
        var _ = function () {
            var r = {}, a = arguments;
            for (var i = 0; i < a.length; i += 2) r[a[i]] = a[i + 1];
            return r;
        }
        var _creator = function () {
            return _(b, _(c, d, e, f, g, d, h, f, i, d, j, k, l, d, m, d, n, f, o, d, p, f), q, _(r, [_(s, t, u, v, w, x, y, z, A, [_(s, B, u, C, w, x, y, D), _(s, E, u, F, w, x, y, G), _(s, H, u, I, w, x, y, J), _(s, K, u, L, w, x, y, M), _(s, N, u, O, w, x, y, P), _(s, Q, u, R, w, x, y, S)])]), T, [U, V, W], X, [Y, Z, ba], bb, _(bc, bd), be, _(bf, _(s, bg, bh, bi, bj, bk, bl, bm, bn, bo, bp, _(bq, br, bs, bt, bu, bv), bw, bx, by, f, bz, bA, bB, bm, bC, bm, bD, bE, bF, f, bG, _(bH, bI, bJ, bI), bK, _(bL, bI, bM, bI), bN, d, bO, f, bP, bg, bQ, _(bq, br, bs, bR), bS, _(bq, br, bs, bT), bU, bV, bW, br, bu, bV, bX, bY, bZ, ca, cb, cc, cd, cc, ce, cc, cf, cc, cg, _(), ch, null, ci, null, cj, bY, ck, _(cl, f, cm, cn, co, cn, cp, cn, bs, _(cq, cr, cs, cr, ct, cr, cu, cv)), cw, _(cl, f, cm, bI, co, cn, cp, cn, bs, _(cq, cr, cs, cr, ct, cr, cu, cv)), cx, _(cl, f, cm, bv, co, bv, cp, cn, bs, _(cq, cr, cs, cr, ct, cr, cu, cy)), cz, cA), cB, _(cC, _(s, cD, bh, cE), cF, _(s, cG, bh, cE, bw, cH, bj, cI, bS, _(bq, br, bs, cJ), bU, bY, bQ, _(bq, br, bs, cJ), bz, cK, bZ, cL, cb, bY, cd, bY, ce, bY, cf, bY), cM, _(s, cN, bh, cE, bw, cO, bj, cI, bS, _(bq, br, bs, cJ), bU, bY, bQ, _(bq, br, bs, cJ), bz, cK, bZ, cL, cb, bY, cd, bY, ce, bY, cf, bY), cP, _(s, cQ, bh, cE, bw, cR, bj, cI, bS, _(bq, br, bs, cJ), bU, bY, bQ, _(bq, br, bs, cJ), bz, cK, bZ, cL, cb, bY, cd, bY, ce, bY, cf, bY), cS, _(s, cT, bh, cE, bw, cU, bj, cI, bS, _(bq, br, bs, cJ), bU, bY, bQ, _(bq, br, bs, cJ), bz, cK, bZ, cL, cb, bY, cd, bY, ce, bY, cf, bY), cV, _(s, cW, bh, cE, bj, cI, bS, _(bq, br, bs, cJ), bU, bY, bQ, _(bq, br, bs, cJ), bz, cK, bZ, cL, cb, bY, cd, bY, ce, bY, cf, bY), cX, _(s, cY, bh, cE, bw, cZ, bj, cI, bS, _(bq, br, bs, cJ), bU, bY, bQ, _(bq, br, bs, cJ), bz, cK, bZ, cL, cb, bY, cd, bY, ce, bY, cf, bY), da, _(s, db, bh, cE, bS, _(bq, br, bs, cJ), bU, bY, bQ, _(bq, br, bs, cJ), bz, cK, bZ, cL, cb, bY, cd, bY, ce, bY, cf, bY), dc, _(s, dd, bp, _(bq, br, bs, de, bu, bv)), df, _(s, dg, bp, _(bq, br, bs, dh, bu, bv), bQ, _(bq, br, bs, di)), dj, _(s, dk, bh, cE, bp, _(bq, br, bs, dh, bu, bv), bz, cK), dl, _(s, dm, bh, cE, bp, _(bq, br, bs, dh, bu, bv), bz, cK, bZ, cL), dn, _(s, dp, bh, cE, bQ, _(bq, br, bs, cJ), bz, cK, bZ, cL), dq, _(s, dr, bh, cE, bp, _(bq, br, bs, dh, bu, bv), bQ, _(bq, br, bs, cJ)), ds, _(s, dt, bh, cE), du, _(s, dv, bh, cE)), dw, _()));
        };
        var b = "configuration", c = "showPageNotes", d = true, e = "showPageNoteNames", f = false,
            g = "showAnnotations", h = "showAnnotationsSidebar", i = "showConsole", j = "linkStyle",
            k = "displayMultipleTargetsOnly", l = "linkFlowsToPages", m = "linkFlowsToPagesNewWindow", n = "useLabels",
            o = "useViews", p = "loadFeedbackPlugin", q = "sitemap", r = "rootNodes", s = "id", t = "zs7kib",
            u = "pageName", v = "数据范围管理首页", w = "type", x = "Wireframe", y = "url", z = "数据范围管理首页.html",
            A = "children", B = "sxi3l6", C = "批量数据授权-1", D = "批量数据授权-1.html", E = "j74l8z",
            F = "批量数据授权-2", G = "批量数据授权-2.html", H = "3808t5", I = "批量数据授权-3",
            J = "批量数据授权-3.html", K = "fb449v", L = "单个数据授权-1", M = "单个数据授权-1.html", N = "rchxty",
            O = "单个数据授权-2", P = "单个数据授权-2.html", Q = "coev8z", R = "单个数据授权-3",
            S = "单个数据授权-3.html", T = "additionalJs", U = "plugins/sitemap/sitemap.js",
            V = "plugins/page_notes/page_notes.js", W = "plugins/debug/debug.js", X = "additionalCss",
            Y = "plugins/sitemap/styles/sitemap.css", Z = "plugins/page_notes/styles/page_notes.css",
            ba = "plugins/debug/styles/debug.css", bb = "globalVariables", bc = "onloadvariable", bd = "",
            be = "stylesheet", bf = "defaultStyle", bg = "627587b6038d43cca051c114ac41ad32", bh = "fontName",
            bi = "'Arial Normal', 'Arial'", bj = "fontWeight", bk = "400", bl = "fontStyle", bm = "normal",
            bn = "fontStretch", bo = "5", bp = "foreGroundFill", bq = "fillType", br = "solid", bs = "color",
            bt = 0xFF333333, bu = "opacity", bv = 1, bw = "fontSize", bx = "13px", by = "underline",
            bz = "horizontalAlignment", bA = "center", bB = "lineSpacing", bC = "characterSpacing", bD = "letterCase",
            bE = "none", bF = "strikethrough", bG = "location", bH = "x", bI = 0, bJ = "y", bK = "size", bL = "width",
            bM = "height", bN = "visible", bO = "limbo", bP = "baseStyle", bQ = "fill", bR = 0xFFFFFFFF,
            bS = "borderFill", bT = 0xFF797979, bU = "borderWidth", bV = "1", bW = "linePattern", bX = "cornerRadius",
            bY = "0", bZ = "verticalAlignment", ca = "middle", cb = "paddingLeft", cc = "2", cd = "paddingTop",
            ce = "paddingRight", cf = "paddingBottom", cg = "stateStyles", ch = "image", ci = "imageFilter",
            cj = "rotation", ck = "outerShadow", cl = "on", cm = "offsetX", cn = 5, co = "offsetY", cp = "blurRadius",
            cq = "r", cr = 0, cs = "g", ct = "b", cu = "a", cv = 0.349019607843137, cw = "innerShadow",
            cx = "textShadow", cy = 0.647058823529412, cz = "viewOverride", cA = "19e82109f102476f933582835c373474",
            cB = "customStyles", cC = "_形状", cD = "40519e9ec4264601bfb12c514e4f4867", cE = "'微软雅黑'",
            cF = "_一级标题", cG = "1111111151944dfba49f67fd55eb1f88", cH = "32px", cI = "bold", cJ = 0xFFFFFF,
            cK = "left", cL = "top", cM = "_二级标题", cN = "b3a15c9ddde04520be40f94c8168891e", cO = "24px",
            cP = "_三级标题", cQ = "8c7a4c5ad69a4369a5f7788171ac0b32", cR = "18px", cS = "_四级标题",
            cT = "e995c891077945c89c0b5fe110d15a0b", cU = "14px", cV = "_五级标题",
            cW = "386b19ef4be143bd9b6c392ded969f89", cX = "_六级标题", cY = "fc3b9a13b5574fa098ef0a1db9aac861",
            cZ = "10px", da = "_文本段落", db = "4988d43d80b44008a4a415096f1632af", dc = "_表单提示",
            dd = "4889d666e8ad4c5e81e59863039a5cc0", de = 0xFF999999, df = "_表单禁用",
            dg = "9bd0236217a94d89b0314c8c7fc75f16", dh = 0xFF000000, di = 0xFFF0F0F0, dj = "_文本框",
            dk = "44157808f2934100b68f2394a66b2bba", dl = "_下拉列表", dm = "85f724022aae41c594175ddac9c289eb",
            dn = "_复选框", dp = "bccdabddb5454e438d4613702b55674b", dq = "_提交按钮",
            dr = "eed12d9ebe2e4b9689b3b57949563dca", ds = "_表格", dt = "a6891ed191e94b3a983da3ba0dc95723",
            du = "_单元格", dv = "33ea2511485c479dbf973af3302f2352", dw = "duplicateStyles";
        return _creator();
    })());