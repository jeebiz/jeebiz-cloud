-- Add/modify columns 
alter table ZFTAL_XTGL_XTSZB
    modify ZDZ VARCHAR2 (200);

-- Add/modify columns 
alter table ZFTAL_XTGL_XTSZB
    add XTSZBLYB VARCHAR2 (30);

/*�����һز�������:Ŀǰ�Ѿ��������Ͷ��ŵĲ���ʵ��*/

delete ZFTAL_PWD_STRATEGY;

insert into ZFTAL_PWD_STRATEGY (STRATEGY_ID, STRATEGY_NAME, STRATEGY_DESC, STRATEGY_STAT)
values ('4CF5C5E0B44A6321E0538713470AD7E8', 'email', 'ͨ���ܱ����䣨ϵͳ�����������˻�Ԥ�����䷢����֤�룬�����Ԥ�������ѡ����',
        '1');

insert into ZFTAL_PWD_STRATEGY (STRATEGY_ID, STRATEGY_NAME, STRATEGY_DESC, STRATEGY_STAT)
values ('4CF5C5E0B44B6321E0538713470AD7E8', 'phone',
        'ͨ���ܱ��ֻ���ϵͳ�����������˻�Ԥ���ֻ����뷢����֤�룬�����Ԥ���ֻ������ѡ����', '1');

insert into ZFTAL_PWD_STRATEGY (STRATEGY_ID, STRATEGY_NAME, STRATEGY_DESC, STRATEGY_STAT)
values ('4CF5C5E0B44C6321E0538713470AD7E8', 'otp', 'ͨ����̬����˻��󶨹���̬���ƣ�', '0');

insert into ZFTAL_PWD_STRATEGY (STRATEGY_ID, STRATEGY_NAME, STRATEGY_DESC, STRATEGY_STAT)
values ('4CF5C5E0B44D6321E0538713470AD7E8', 'one-card', 'ͨ��һ��ͨ���ѿ�ͨ���󶨹�һ��ͨ��', '0');

insert into ZFTAL_PWD_STRATEGY (STRATEGY_ID, STRATEGY_NAME, STRATEGY_DESC, STRATEGY_STAT)
values ('4CF5C5E0B44E6321E0538713470AD7E8', 'sq', 'ͨ���ܱ����⣨���ù��ܱ����⣩', '0');

insert into ZFTAL_PWD_STRATEGY (STRATEGY_ID, STRATEGY_NAME, STRATEGY_DESC, STRATEGY_STAT)
values ('4CF5C5E0B44F6321E0538713470AD7E8', 'appeal', 'ͨ�����ߣ����Ϸ�ʽ�����ʺ��ң�', '0');


/*�����һ��������*/
delete zftal_xtgl_xtszb t
where t.zdm in ('pwd.mail.subject', 'pwd.mail.content', 'pwd.sms.content');
/*ָ�������һط����ʼ�������*/
insert into zftal_xtgl_xtszb (ZDM, ZDZ, SSMK, ZS, BZ, SSGNMKDM, ZDZYQ, ZDLX, ZDLY)
values ('pwd.mail.subject', '�����һ�', 'PWDMGR', '�ʼ�����', '�������Ϊ�ʼ�������', '', '{required:true}', 2, '');
/*ָ�������һط����ʼ�������*/
insert into zftal_xtgl_xtszb (ZDM, ZDZ, SSMK, ZS, BZ, SSGNMKDM, ZDZYQ, ZDLX, ZDLY)
values ('pwd.mail.content', '����ǰͨ�������һ��������֤��Ϊ��#{map.captcha},�뾡����֤!', 'PWDMGR', '�ʼ�����',
        '�Զ����ʼ���ģ�壬���ñ��� #{map.captcha} ��ʾ��֤��.', '', '{required:true}', 2, '');
/*ָ�������һط��Ͷ��ŵ�����*/
insert into zftal_xtgl_xtszb (ZDM, ZDZ, SSMK, ZS, BZ, SSGNMKDM, ZDZYQ, ZDLX, ZDLY)
values ('pwd.sms.content', '����ǰͨ���ֻ��һ��������֤��Ϊ��#{map.captcha},�뾡����֤!', 'PWDMGR', '��������',
        '�Զ�����ŵ�ģ�壬���ñ��� #{map.captcha} ��ʾ��֤��.', '', '{required:true}', 2, '');


/*�����һ�*/

delete zftal_xtgl_jsgnmkdmb
where gnmkdm in ('N0155', 'N015501', 'N015505', 'N015510');

insert into zftal_xtgl_jsgnmkdmb(GNMKDM, GNMKMC, FJGNDM, DYYM, XSSX, CDJB, GNSYDX)
values ('N0155', '�����һ�', 'N01', '', '1', '1', 'gl');

/*�һط�ʽ*/
insert into zftal_xtgl_jsgnmkdmb(GNMKDM, GNMKMC, FJGNDM, DYYM, XSSX, SFXS, SFZDYMK, XSLX)
values ('N015501', '�һط�ʽ', 'N0155', '/pwdmgr/setting/strategy.zf', '1', '1', '0', '0');

delete
from zftal_xtgl_gnmkczb
where GNMKDM = 'N015501';
insert into zftal_xtgl_gnmkczb(GNMKDM, CZDM, SFXS, SFZDYCZ)
values ('N015501', 'xg', '1', '0');
insert into zftal_xtgl_gnmkczb(GNMKDM, CZDM, SFXS, SFZDYCZ)
values ('N015501', 'cx', '0', '0');

delete
from zftal_xtgl_jsgnmkczb
where GNMKDM like 'N015501%';
insert into zftal_xtgl_jsgnmkczb(JSDM, GNMKDM, CZDM)
values ('admin', 'N015501', 'xg');
insert into zftal_xtgl_jsgnmkczb(JSDM, GNMKDM, CZDM)
values ('admin', 'N015501', 'cx');

/*��ʵ��Ϣ*/
insert into zftal_xtgl_jsgnmkdmb(GNMKDM, GNMKMC, FJGNDM, DYYM, XSSX, SFXS, SFZDYMK, XSLX)
values ('N015505', '��ʵ��Ϣ', 'N0155', '/pwdmgr/setting/verifi.zf', '1', '1', '0', '0');

delete
from zftal_xtgl_gnmkczb
where GNMKDM = 'N015505';
insert into zftal_xtgl_gnmkczb(GNMKDM, CZDM, SFXS, SFZDYCZ)
values ('N015505', 'zj', '1', '0');
insert into zftal_xtgl_gnmkczb(GNMKDM, CZDM, SFXS, SFZDYCZ)
values ('N015505', 'bc', '1', '0');
insert into zftal_xtgl_gnmkczb(GNMKDM, CZDM, SFXS, SFZDYCZ)
values ('N015505', 'sc', '1', '0');
insert into zftal_xtgl_gnmkczb(GNMKDM, CZDM, SFXS, SFZDYCZ)
values ('N015505', 'cx', '0', '0');

delete
from zftal_xtgl_jsgnmkczb
where GNMKDM like 'N015505%';
insert into zftal_xtgl_jsgnmkczb(JSDM, GNMKDM, CZDM)
values ('admin', 'N015505', 'zj');
insert into zftal_xtgl_jsgnmkczb(JSDM, GNMKDM, CZDM)
values ('admin', 'N015505', 'bc');
insert into zftal_xtgl_jsgnmkczb(JSDM, GNMKDM, CZDM)
values ('admin', 'N015505', 'sc');
insert into zftal_xtgl_jsgnmkczb(JSDM, GNMKDM, CZDM)
values ('admin', 'N015505', 'cx');

/*���ò���*/
insert into zftal_xtgl_jsgnmkdmb(GNMKDM, GNMKMC, FJGNDM, DYYM, XSSX, SFXS, SFZDYMK, XSLX)
values ('N015510', '���ò���', 'N0155', '/pwdmgr/setting/props.zf', '1', '1', '0', '0');

delete
from zftal_xtgl_gnmkczb
where GNMKDM = 'N015510';
insert into zftal_xtgl_gnmkczb(GNMKDM, CZDM, SFXS, SFZDYCZ)
values ('N015510', 'cx', '0', '0');
insert into zftal_xtgl_gnmkczb(GNMKDM, CZDM, SFXS, SFZDYCZ)
values ('N015510', 'xg', '1', '0');

delete
from zftal_xtgl_jsgnmkczb
where GNMKDM like 'N015510%';
insert into zftal_xtgl_jsgnmkczb(JSDM, GNMKDM, CZDM)
values ('admin', 'N015510', 'cx');
insert into zftal_xtgl_jsgnmkczb(JSDM, GNMKDM, CZDM)
values ('admin', 'N015510', 'xg');

commit;
