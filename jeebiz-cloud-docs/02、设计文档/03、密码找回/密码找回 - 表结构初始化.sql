-- Create table
create table ZFTAL_PWD_STRATEGY
(
    STRATEGY_ID VARCHAR2 (32) default sys_guid() not null,
    STRATEGY_NAME VARCHAR2 (30),
    STRATEGY_DESC VARCHAR2 (200),
    STRATEGY_STAT VARCHAR2 (2)
);
-- Add comments to the table 
comment on table ZFTAL_PWD_STRATEGY  is '�����һز��Ա�';
-- Add comments to the columns 
comment on column ZFTAL_PWD_STRATEGY.STRATEGY_ID  is '���Ա�ID';
comment on column ZFTAL_PWD_STRATEGY.STRATEGY_NAME  is '�������ƣ������Ʊ��������ʵ�ֶ���name�����ṩ�ķ���ֵһ��';
comment on column ZFTAL_PWD_STRATEGY.STRATEGY_DESC  is '���������������ò��Ե�ʵ�ַ�ʽ';
comment on column ZFTAL_PWD_STRATEGY.STRATEGY_STAT  is '����״̬��ǣ�1������(��״̬��ϵͳ��Ҫ����name��Ӧ�Ĳ���ʵ��,������Ч)��0��ͣ��';
-- Create/Recreate primary, unique and foreign key constraints 
alter table ZFTAL_PWD_STRATEGY
    add constraint PK_ZFTAL_PWD_STRATEGY primary key (STRATEGY_ID);

-- Create table
create table ZFTAL_PWD_VERIFI
(
    VERIFI_ID VARCHAR2 (32) default sys_guid() not null,
    VERIFI_NAME VARCHAR2 (30),
    VERIFI_LABEL VARCHAR2 (30),
    VERIFI_DESC VARCHAR2 (200),
    VERIFI_RULES VARCHAR2 (200),
    REQUIRED VARCHAR2 (2),
    VERIFI_STAT VARCHAR2 (2)
);
-- Add comments to the table 
comment on table ZFTAL_PWD_VERIFI  is '�˺ź�ʵ�ֶα�';
-- Add comments to the columns 
comment on column ZFTAL_PWD_VERIFI.VERIFI_ID  is '�˺ź�ʵ�ֶα�ID';
comment on column ZFTAL_PWD_VERIFI.VERIFI_NAME  is '�˺ź�ʵ�ֶ�����';
comment on column ZFTAL_PWD_VERIFI.VERIFI_LABEL  is '�˺ź�ʵ�ֶ�Label����';
comment on column ZFTAL_PWD_VERIFI.VERIFI_DESC  is '�˺ź�ʵ�ֶ���������Ϊ��ʾ��Ϣ';
comment on column ZFTAL_PWD_VERIFI.VERIFI_RULES  is '�˺ź�ʵ�ֶ�У�����';
comment on column ZFTAL_PWD_VERIFI.REQUIRED  is '�˺ź�ʵ�ֶ��Ƿ���1���ǣ�0����';
comment on column ZFTAL_PWD_VERIFI.VERIFI_STAT  is '�˺ź�ʵ�ֶ�����״̬��ǣ�1�����ã�0��ͣ��';
-- Create/Recreate primary, unique and foreign key constraints 
alter table ZFTAL_PWD_VERIFI
    add constraint PK_ZFTAL_PWD_VERIFI primary key (VERIFI_ID);

