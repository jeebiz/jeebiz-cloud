delete zftal_xtgl_jsgnmkdmb a
where a.gnmkdm in (
    select distinct t.gnmkdm
    from zftal_xtgl_jsgnmkdmb t
    where t.sfxs <> '0'
    start with t.gnmkdm = 'N0120'
    connect by prior t.gnmkdm = t.fjgndm);

delete zftal_xtgl_jsgnmkczb a
where a.gnmkdm like 'N0120%';
delete zftal_xtgl_gnmkczb a
where a.gnmkdm like 'N0120%';

delete
from ZFTAL_XTGL_CZDMB
where czdm in ('bs', 'fz');
insert into ZFTAL_XTGL_CZDMB(czdm, czmc, xssx, anys)
values ('bs', '部署', '99', 'glyphicon glyphicon-cog');
insert into ZFTAL_XTGL_CZDMB(czdm, czmc, xssx, anys)
values ('fz', '复制', '99', 'glyphicon glyphicon-file');


/*流程管理*/

insert into zftal_xtgl_jsgnmkdmb (GNMKDM, GNMKMC, FJGNDM, DYYM, XSSX, CDJB, GNSYDX, YYTPLJ, MHGNMKDM, TBMHDZ, SFXS,
                                  SFZDYMK, GNMKYWMC, TBLJ, GNMKJC, GNMKYWJC)
values ('N0120', '流程管理', 'N01', '', '20', 2, 'gl', '', '', '', '1', '0', 'Workflow Settings', '', '', '');


/*流程定义*/

insert into zftal_xtgl_jsgnmkdmb (GNMKDM, GNMKMC, FJGNDM, DYYM, XSSX, CDJB, GNSYDX, YYTPLJ, MHGNMKDM, TBMHDZ, SFXS,
                                  SFZDYMK, GNMKYWMC, TBLJ, GNMKJC, GNMKYWJC)
values ('N012001', '流程定义', 'N0120', '/processManagement/moduler/list.zf', '1', 3, '', '', '', '', '1', '0',
        'Process Definition', '', '', '');

insert into zftal_xtgl_gnmkczb (GNMKDM, CZDM, SFXS, SFZDYCZ, CZMC, YWMC)
values ('N012001', 'cx', '0', '0', '', '');
insert into zftal_xtgl_gnmkczb (GNMKDM, CZDM, SFXS, SFZDYCZ, CZMC, YWMC)
values ('N012001', 'bs', '1', '0', '', '');
insert into zftal_xtgl_gnmkczb (GNMKDM, CZDM, SFXS, SFZDYCZ, CZMC, YWMC)
values ('N012001', 'ck', '1', '0', '', '');
insert into zftal_xtgl_gnmkczb (GNMKDM, CZDM, SFXS, SFZDYCZ, CZMC, YWMC)
values ('N012001', 'fz', '1', '0', '', '');
insert into zftal_xtgl_gnmkczb (GNMKDM, CZDM, SFXS, SFZDYCZ, CZMC, YWMC)
values ('N012001', 'sc', '1', '0', '', '');
insert into zftal_xtgl_gnmkczb (GNMKDM, CZDM, SFXS, SFZDYCZ, CZMC, YWMC)
values ('N012001', 'xg', '1', '0', '', '');
insert into zftal_xtgl_gnmkczb (GNMKDM, CZDM, SFXS, SFZDYCZ, CZMC, YWMC)
values ('N012001', 'zj', '1', '0', '', '');

insert into zftal_xtgl_jsgnmkczb (JSDM, GNMKDM, CZDM, DYYM)
values ('admin', 'N012001', 'cx', '');
insert into zftal_xtgl_jsgnmkczb (JSDM, GNMKDM, CZDM, DYYM)
values ('admin', 'N012001', 'bs', '');
insert into zftal_xtgl_jsgnmkczb (JSDM, GNMKDM, CZDM, DYYM)
values ('admin', 'N012001', 'ck', '');
insert into zftal_xtgl_jsgnmkczb (JSDM, GNMKDM, CZDM, DYYM)
values ('admin', 'N012001', 'fz', '');
insert into zftal_xtgl_jsgnmkczb (JSDM, GNMKDM, CZDM, DYYM)
values ('admin', 'N012001', 'sc', '');
insert into zftal_xtgl_jsgnmkczb (JSDM, GNMKDM, CZDM, DYYM)
values ('admin', 'N012001', 'xg', '');
insert into zftal_xtgl_jsgnmkczb (JSDM, GNMKDM, CZDM, DYYM)
values ('admin', 'N012001', 'zj', '');

/*流程部署*/

insert into zftal_xtgl_jsgnmkdmb (GNMKDM, GNMKMC, FJGNDM, DYYM, XSSX, CDJB, GNSYDX, YYTPLJ, MHGNMKDM, TBMHDZ, SFXS,
                                  SFZDYMK, GNMKYWMC, TBLJ, GNMKJC, GNMKYWJC)
values ('N012005', '流程部署', 'N0120', '/processDefinition/list.zf', '2', 3, 'gl', '', '', '', '1', '0',
        'Process Deployment', '', '', '');

insert into zftal_xtgl_gnmkczb (GNMKDM, CZDM, SFXS, SFZDYCZ, CZMC, YWMC)
values ('N012005', 'cx', '0', '0', '', '');
insert into zftal_xtgl_gnmkczb (GNMKDM, CZDM, SFXS, SFZDYCZ, CZMC, YWMC)
values ('N012005', 'ck', '1', '0', '', '');
insert into zftal_xtgl_gnmkczb (GNMKDM, CZDM, SFXS, SFZDYCZ, CZMC, YWMC)
values ('N012005', 'sc', '1', '0', '', '');
insert into zftal_xtgl_gnmkczb (GNMKDM, CZDM, SFXS, SFZDYCZ, CZMC, YWMC)
values ('N012005', 'qy', '1', '0', '', '');
insert into zftal_xtgl_gnmkczb (GNMKDM, CZDM, SFXS, SFZDYCZ, CZMC, YWMC)
values ('N012005', 'ty', '1', '0', '', '');
insert into zftal_xtgl_gnmkczb (GNMKDM, CZDM, SFXS, SFZDYCZ, CZMC, YWMC)
values ('N012005', 'assignment', '1', '0', '', '');

insert into zftal_xtgl_jsgnmkczb (JSDM, GNMKDM, CZDM, DYYM)
values ('admin', 'N012005', 'cx', '');
insert into zftal_xtgl_jsgnmkczb (JSDM, GNMKDM, CZDM, DYYM)
values ('admin', 'N012005', 'ck', '');
insert into zftal_xtgl_jsgnmkczb (JSDM, GNMKDM, CZDM, DYYM)
values ('admin', 'N012005', 'sc', '');
insert into zftal_xtgl_jsgnmkczb (JSDM, GNMKDM, CZDM, DYYM)
values ('admin', 'N012005', 'qy', '');
insert into zftal_xtgl_jsgnmkczb (JSDM, GNMKDM, CZDM, DYYM)
values ('admin', 'N012005', 'ty', '');
insert into zftal_xtgl_jsgnmkczb (JSDM, GNMKDM, CZDM, DYYM)
values ('admin', 'N012005', 'assignment', '');

insert into ZFTAL_XTGL_CZDMB(cddm, cdmc, xssx, anys)
values ('assignment', '办理人设置', '90', 'glyphicon glyphicon-user');

commit;



/*DROP TABLE ACT_ID_GROUP;
DROP TABLE ACT_ID_USER;
DROP TABLE ACT_ID_MEMBERSHIP;
*/
create or replace view ACT_ID_GROUP as
select jsdm as ID_, 1 as REV_, jsmc as NAME_, 'assignment' as TYPE_
from zftal_xtgl_jsxxb
where jsdm <> 'admin'
with READ ONLY;
/

create or replace view ACT_ID_USER as
select yhm as ID_, 1 as REV_, xm as FIRST_, null as LAST_, dzyx as EMAIL_, null as PWD_, null as PICTURE_ID_
from zftal_xtgl_yhb
where yhm <> 'admin'
with READ ONLY;
/

create or replace view ACT_ID_MEMBERSHIP as
select jsdm as GROUP_ID_, yhm as USER_ID_
from zftal_xtgl_yhjsb
where yhm <> 'admin'
with READ ONLY;
/

CREATE OR REPLACE VIEW V_ASSIGNED_TASKLIST AS
select distinct A.ID_                                            AS P_TASK_ID,
                A.PROC_INST_ID_                                     P_PROC_INST_ID,
                A.TASK_DEF_KEY_                                  AS P_ACT_ID,
                A.NAME_                                          AS P_ACT_NAME,
                A.OWNER_                                         AS P_OWNER,
                A.ASSIGNEE_                                      AS P_ASSIGNEE,
                A.DELEGATION_                                    AS P_DELEGATION,
                A.DESCRIPTION_                                   AS P_DESCRIPTION,
                TO_CHAR(A.CREATE_TIME_, 'YYYY-MM-DD HH24:MI:SS') AS P_CREATE_TIME,
                TO_CHAR(A.DUE_DATE_, 'YYYY-MM-DD HH24:MI:SS')    AS P_DUE_DATE
from ACT_RU_TASK A
         inner join ACT_RE_PROCDEF D
                    on A.PROC_DEF_ID_ = D.ID_
WHERE A.ASSIGNEE_ IS NOT NULL
order by A.ID_ asc;
/

CREATE or replace VIEW V_QUEUE_TASKLIST AS
SELECT A.ID_                                            AS P_TASK_ID,
       A.PROC_INST_ID_                                  AS P_PROC_INST_ID,
       A.TASK_DEF_KEY_                                  AS P_ACT_ID,
       A.NAME_                                          AS P_ACT_NAME,
       A.OWNER_                                         AS P_OWNER,
       A.ASSIGNEE_                                      AS P_ASSIGNEE,
       A.DELEGATION_                                    AS P_DELEGATION,
       A.DESCRIPTION_                                   AS P_DESCRIPTION,
       TO_CHAR(A.CREATE_TIME_, 'YYYY-MM-DD HH24:MI:SS') AS P_CREATE_TIME,
       TO_CHAR(A.DUE_DATE_, 'YYYY-MM-DD HH24:MI:SS')    AS P_DUE_DATE,
       I.USER_ID                                           P_CANDIDATE
FROM ACT_RU_TASK A
         INNER JOIN (SELECT DISTINCT *
                     FROM (SELECT TASK_ID_, TO_CHAR(USER_ID_) USER_ID
                           FROM ACT_RU_IDENTITYLINK I,
                                ACT_RU_TASK T
                           WHERE TASK_ID_ IS NOT NULL
                             AND USER_ID_ IS NOT NULL
                             AND I.TASK_ID_ = T.ID_
                             AND T.ASSIGNEE_ IS NULL
                             AND TYPE_ = 'candidate'
                           UNION
                           SELECT TASK_ID_, R.USER_ID_
                           FROM ACT_RU_IDENTITYLINK I,
                                act_id_membership R,
                                ACT_RU_TASK T
                           WHERE I.TASK_ID_ IS NOT NULL
                             AND I.GROUP_ID_ IS NOT NULL
                             AND I.TASK_ID_ = T.ID_
                             AND T.ASSIGNEE_ IS NULL
                             AND TYPE_ = 'candidate'
                             AND I.GROUP_ID_ = R.GROUP_ID_) U) I
                    ON A.ID_ = I.TASK_ID_;
/

CREATE or replace VIEW V_TASKLIST AS
SELECT A.ID_                                            AS P_TASK_ID,
       A.PROC_INST_ID_                                  AS P_PROC_INST_ID,
       A.TASK_DEF_KEY_                                  AS P_ACT_ID,
       A.NAME_                                          AS P_ACT_NAME,
       A.OWNER_                                         AS P_OWNER,
       A.ASSIGNEE_                                      AS P_ASSIGNEE,
       A.DELEGATION_                                    AS P_DELEGATION,
       A.DESCRIPTION_                                   AS P_DESCRIPTION,
       TO_CHAR(A.CREATE_TIME_, 'YYYY-MM-DD HH24:MI:SS') AS P_CREATE_TIME,
       TO_CHAR(A.DUE_DATE_, 'YYYY-MM-DD HH24:MI:SS')    AS P_DUE_DATE,
       I.USER_ID                                           P_CANDIDATE
FROM ACT_RU_TASK A
         INNER JOIN (SELECT DISTINCT *
                     FROM (SELECT TASK_ID_, TO_CHAR(USER_ID_) USER_ID
                           FROM ACT_RU_IDENTITYLINK I,
                                ACT_RU_TASK T
                           WHERE TASK_ID_ IS NOT NULL
                             AND USER_ID_ IS NOT NULL
                             AND I.TASK_ID_ = T.ID_
                             AND T.ASSIGNEE_ IS NULL
                             AND TYPE_ = 'candidate'
                           UNION
                           SELECT TASK_ID_, R.USER_ID_
                           FROM ACT_RU_IDENTITYLINK I,
                                act_id_membership R,
                                ACT_RU_TASK T
                           WHERE I.TASK_ID_ IS NOT NULL
                             AND I.GROUP_ID_ IS NOT NULL
                             AND I.TASK_ID_ = T.ID_
                             AND T.ASSIGNEE_ IS NULL
                             AND TYPE_ = 'candidate'
                             AND I.GROUP_ID_ = R.GROUP_ID_
                           UNION
                           SELECT T.ID_                TASK_ID_,
                                  TO_CHAR(T.ASSIGNEE_) USER_ID
                           FROM ACT_RU_TASK T
                           WHERE T.ASSIGNEE_ IS NOT NULL) U) I
                    ON A.ID_ = I.TASK_ID_;
/

CREATE or replace VIEW V_HIS_TASKLIST AS
SELECT a.proc_inst_id_                                    P_PROC_INST_ID,
       a.proc_def_id_                                     P_PROC_DEF_ID,
       a.task_def_key_                                    P_TASK_DEF_ID,
       a.id_                                              P_TASK_ID,
       a.name_                                            P_TASK_NAME,
       a.assignee_                                        P_ASSIGNEE,
       a.owner_                                           P_OWNER,
       a.description_                                     P_DESCRIPTION,
       a.category_                                        P_TASK_CATEGORY,
       a.duration_                                        P_DURATION,
       TO_CHAR(A.CLAIM_TIME_, 'YYYY-MM-DD HH24:MI:SS') AS P_CLAIM_DATE,
       TO_CHAR(A.START_TIME_, 'YYYY-MM-DD HH24:MI:SS') AS P_START_TIME,
       TO_CHAR(A.END_TIME_, 'YYYY-MM-DD HH24:MI:SS')   AS P_END_TIME,
       b.message_                                      as P_TASK_MESSAGE,
       b.full_msg_                                     as P_TASK_FULL_MESSAGE
FROM (SELECT row_number() OVER (PARTITION BY proc_inst_id_, assignee_ ORDER BY end_time_ DESC) LEV, t.*
      FROM act_hi_taskinst t
      WHERE t.assignee_ IS NOT NULL
        and t.end_time_ IS NOT NULL) a
         LEFT JOIN act_hi_comment b
                   ON a.proc_inst_id_ = b.proc_inst_id_ and a.id_ = b.task_id_ and b.type_ = 'process_log'
WHERE LEV = 1
ORDER BY a.END_TIME_;
/

create or replace VIEW V_PROCESS_TRACE AS
select *
from (SELECT a.act_id_                                       as p_act_id,
             a.act_name_                                     as p_act_name,
             a.act_type_                                     as p_act_type,
             a.assignee_                                     as p_assignee,
             a.duration_                                     as p_duration,
             TO_CHAR(a.end_time_, 'YYYY-MM-DD HH24:MI:SS')   as p_end_time,
             TO_CHAR(a.start_time_, 'YYYY-MM-DD HH24:MI:SS') as p_start_time,
             a.proc_def_id_                                  as p_proc_def_id,
             a.proc_inst_id_                                 as p_proc_inst_id,
             a.task_id_                                      as p_task_id,
             b.message_                                      as p_task_comment,
             b.full_msg_                                     as P_TASK_FULL_COMMENT
      FROM act_hi_actinst a
               left join act_hi_comment b on a.task_id_ = b.task_id_
      where a.end_time_ is not null
        and a.act_type_ in ('startEvent', 'userTask', 'endEvent'))
order by p_end_time asc;
/
   
--updated 2016/12/29
CREATE OR REPLACE VIEW V_PROCESS_TRACE AS
select "P_ACT_ID",
       "P_ACT_NAME",
       "P_ACT_TYPE",
       "P_ASSIGNEE",
       "P_USER_ID",
       "P_DURATION",
       "P_END_TIME",
       "P_START_TIME",
       "P_PROC_DEF_ID",
       "P_PROC_INST_ID",
       "P_TASK_ID",
       "P_TASK_COMMENT",
       "P_TASK_FULL_COMMENT"
from (SELECT a.act_id_                                       as p_act_id,
             a.act_name_                                     as p_act_name,
             a.act_type_                                     as p_act_type,
             a.assignee_                                     as p_assignee,
             a.duration_                                     as p_duration,
             TO_CHAR(a.end_time_, 'YYYY-MM-DD HH24:MI:SS')   as p_end_time,
             TO_CHAR(a.start_time_, 'YYYY-MM-DD HH24:MI:SS') as p_start_time,
             a.proc_def_id_                                  as p_proc_def_id,
             a.proc_inst_id_                                 as p_proc_inst_id,
             a.task_id_                                      as p_task_id,
             b.user_id_                                      as p_user_id,
             b.message_                                      as p_task_comment,
             b.full_msg_                                     as P_TASK_FULL_COMMENT
      FROM act_hi_actinst a
               left join act_hi_comment b on a.task_id_ = b.task_id_
      where a.end_time_ is not null
        and a.act_type_ in ('startEvent', 'userTask', 'endEvent'))
order by p_end_time asc;
/
   
 --updated 2016/12/29
-- Create table
create table ACT_RE_ASSIGNMENT
(
    id_ NVARCHAR2(64) not null,
    group_id_ NVARCHAR2(255),
    user_id_ NVARCHAR2(255),
    type_ NVARCHAR2(255) not null,
    task_def_id_ NVARCHAR2(64) not null,
    proc_def_id_ NVARCHAR2(64) not null
);
-- Add comments to the table 
comment on table ACT_RE_ASSIGNMENT
  is '流程任务办理人管理表（扩展表，activiti不提供该表）';
-- Add comments to the columns 
comment on column ACT_RE_ASSIGNMENT.id_
  is '主键';
comment on column ACT_RE_ASSIGNMENT.group_id_
  is '组ID';
comment on column ACT_RE_ASSIGNMENT.user_id_
  is '用户ID';
comment on column ACT_RE_ASSIGNMENT.type_
  is '类型';
comment on column ACT_RE_ASSIGNMENT.task_def_id_
  is '流程任务定义ID';
comment on column ACT_RE_ASSIGNMENT.proc_def_id_
  is '流程定义ID';


-- Create/Recreate indexes 
create index ACT_IDX_RE_AST_TASKDEFID on ACT_RE_ASSIGNMENT (task_def_id_);
-- Create/Recreate primary, unique and foreign key constraints 
alter table ACT_RE_ASSIGNMENT
    add primary key (ID_);


